package com.backend.projeto.repository;

import com.backend.projeto.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserReposity extends JpaRepository<User, Long> {

    User findByEmail(String userName);
}

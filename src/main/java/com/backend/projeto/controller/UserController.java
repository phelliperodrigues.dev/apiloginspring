package com.backend.projeto.controller;

import com.backend.projeto.entity.User;
import com.backend.projeto.repository.UserReposity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserReposity userReposity;

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<User> list(@RequestParam("page") int page,
                           @RequestParam("size") int size){

        return userReposity.findAll(PageRequest.of(page, size));
    }


    @RequestMapping(value = "", method = RequestMethod.POST)
    public User save(@RequestBody User user){
        return userReposity.save(user);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public User edit(@RequestBody User user){
        return userReposity.save(user);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id){
        userReposity.deleteById(id);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Optional<User> detail(@PathVariable Long id){
        return userReposity.findById(id);
    }

}

package com.backend.projeto.config;

import com.backend.projeto.entity.Role;
import com.backend.projeto.entity.User;
import com.backend.projeto.repository.RoleRepository;
import com.backend.projeto.repository.UserReposity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DataInitalizr implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserReposity userReposity;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        List<User> users = userReposity.findAll();

        if(users.isEmpty()){
            this.createUser("Phellipe", "phelliperodrigues.dev@gmail.com",passwordEncoder.encode("123456"), "ROLE_ALUNO");
            this.createUser("Phellipe", "admin",passwordEncoder.encode("123456"),"ROLE_ADMIN");
        }

    }

    public void createUser(String name, String email, String password, String role){
        Role roleObject = new Role();
        roleObject.setName(role);
        this.roleRepository.save(roleObject);

        User user = new User(name, email, password, Arrays.asList(roleObject));
        userReposity.save(user);
    }
}
